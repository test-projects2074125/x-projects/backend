import { Sex } from "@x-projects/app/utils/constants/common.enums";
import { User, UserCard } from "@x-projects/app/interfaces/entities/user-entity.interface";

/** Auth **/
// Signup
export namespace Signup {
  export interface ReqBody {
    name: string;
    email: string;
    password: string;
    birthday: string;
    sex: Sex;
  }

  export type ResBody = {
    success: boolean;
  };
}

// Login
export namespace LogIn {
  export interface ReqBody {
    email: string;
    password: string;
  }

  export type ResBody = {
    user: User;
    accessToken: string;
  };
}

/** Account **/
// Edit account
export namespace EditAccount {
  export interface ReqBody {
    name?: string;
    password?: string;
  }

  export type ResBody = User;
}

// List users
export namespace ListUsers {
  export type ResBody = {
    total: number;
    rows: UserCard[];
  };
}
