import type { Request as ExpressRequest } from "express";

export interface JwtAuthPayload {
  id: string;
  email: string;
}

export type Request<T extends object = any> = ExpressRequest<any, any, T>;
