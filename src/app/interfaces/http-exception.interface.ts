import { HTTP_STATUS } from "@x-projects/app/utils/constants/http-status.constant";

type HttpStatusKeys = keyof typeof HTTP_STATUS;
type HttpStatusValues = (typeof HTTP_STATUS)[HttpStatusKeys];

export interface IHttpException {
  status: HttpStatusValues;
  message: string;
}
