import { Sex } from "@x-projects/app/utils/constants/common.enums";

export interface User {
  id: string;
  name: string;
  email: string;
  birthday: string;
  sex: Sex;
  imagePath: string | null;
}

export interface UserWithPassword extends User {
  password: string;
}

export interface UserCard {
  id: string;
  name: string;
  birthday: string;
  imagePath: string | null;
}
