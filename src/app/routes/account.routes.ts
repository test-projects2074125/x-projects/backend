import * as express from "express";
import { validate, Joi } from "express-validation";
// Utils
import { UserImageMulter } from "@x-projects/app/utils/file-upload.util";
import { verifyJwtToken } from "@x-projects/app/common/middleware/jwt-auth.middleware";
import { setValidationErrorMessage } from "@x-projects/app/utils/error-handler.util";
// Controllers
import { AccountController } from "@x-projects/app/controllers/account.controller";
// Types and Enums
import { Sex } from "@x-projects/app/utils/constants/common.enums";

const router = express.Router();

/** Validations **/
// Signup validation
export const signupValidation = {
  body: Joi.object({
    name: Joi.string()
      .min(3)
      .max(20)
      .required()
      .error(setValidationErrorMessage("Введите корректное имя")),
    email: Joi.string()
      .email()
      .required()
      .error(setValidationErrorMessage("Введите корректный эл. адрес")),
    password: Joi.string()
      .min(6)
      .max(20)
      .required()
      .error(setValidationErrorMessage("Должен содержать не менее 6 символов")),
    birthday: Joi.string()
      .isoDate()
      .required()
      .error(setValidationErrorMessage("Выберите дату рождения")),
    sex: Joi.string()
      .allow(Sex.Male, Sex.Female)
      .required()
      .error(setValidationErrorMessage("Выберите дату рождения")),
  }),
};
// Login validation
export const loginValidation = {
  body: Joi.object({
    email: Joi.string()
      .email()
      .required()
      .error(setValidationErrorMessage("Введите корректный эл. адрес")),
    password: Joi.string()
      .min(6)
      .max(20)
      .required()
      .error(setValidationErrorMessage("Введите корректный парль")),
  }),
};
// Signup validation
export const editAccountValidation = {
  body: Joi.object({
    name: Joi.string()
      .min(3)
      .max(20)
      .optional()
      .error(setValidationErrorMessage("Введите корректное имя")),
    password: Joi.string()
      .min(6)
      .max(20)
      .optional()
      .error(setValidationErrorMessage("Должен содержать не менее 6 символов")),
  }),
};

/** Routes **/
// Auth routes
router.post(
  "/auth/signup",
  UserImageMulter.single("image"),
  validate(signupValidation, {}, { allowUnknown: true, abortEarly: false }),
  AccountController.signup,
);
router.post(
  "/auth/login",
  validate(loginValidation, {}, { allowUnknown: true, abortEarly: false }),
  AccountController.login,
);
router.post("/auth/check", verifyJwtToken, AccountController.checkAuth);

// Account routes
router.put(
  "/account/edit",
  verifyJwtToken,
  UserImageMulter.single("image"),
  validate(editAccountValidation, {}, { allowUnknown: true, abortEarly: false }),
  AccountController.editAccount,
);
router.get("/account/users", verifyJwtToken, AccountController.listUsers);

export { router as accountRouter };
