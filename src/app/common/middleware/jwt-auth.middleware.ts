import * as process from "node:process";
import * as jwt from "jsonwebtoken";
import { NextFunction, Request, Response } from "express";
// Utils
import { isNil } from "lodash";
import { HTTP_STATUS } from "@x-projects/app/utils/constants/http-status.constant";
// Types
import type { JwtAuthPayload } from "@x-projects/app/interfaces/common.interface";

export function verifyJwtToken(req: Request, res: Response, next: NextFunction): any {
  try {
    const accessToken = req.headers["authorization"]?.split(" ")[1];

    if (!accessToken) {
      throw new Error();
    }

    jwt.verify(accessToken, process.env.JWT_ACCESS_TOKEN_SECRET, (err, user) => {
      if (!isNil(err)) {
        res.status(HTTP_STATUS.UNAUTHORIZED).send({ message: "Не авторизован" });
        //
        return;
      }
      req.user = {
        id: (user as JwtAuthPayload).id,
        email: (user as JwtAuthPayload).email,
      };

      return next();
    });
  } catch (err) {
    return res.status(HTTP_STATUS.UNAUTHORIZED).send({ message: "Не авторизован" });
  }
}
