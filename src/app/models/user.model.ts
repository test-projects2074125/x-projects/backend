import * as mongoose from "mongoose";
// Types
import type { UserWithPassword } from "@x-projects/app/interfaces/entities/user-entity.interface";

const userSchema = new mongoose.Schema<UserWithPassword>(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    birthday: {
      type: String,
      required: true,
    },
    sex: {
      type: String,
      enum: ["M", "W"],
      required: true,
    },
    imagePath: {
      type: String,
      nullable: true,
      required: false,
    },
  },
  {
    collection: "users",
    timestamps: false,
    versionKey: false,
    toJSON: {
      transform: (_doc, ret) => {
        ret.id = (ret._id ?? ret.id).toString();
        //
        delete ret.password;
        delete ret._id;
        //
        return ret;
      },
    },
  },
);

const UserModel = mongoose.model("User", userSchema);

export { UserModel };
