import "module-alias/register";
import * as process from "node:process";
import * as express from "express";
// Utils
import * as appRootPath from "app-root-path";
import * as dotenv from "dotenv";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as cors from "cors";
import { isEmpty } from "lodash";
import { appProcessEnvValidationSchema } from "@x-projects/envs/environment.interface";
import { errorHandler } from "@x-projects/app/utils/error-handler.util";
// Routes
import { accountRouter } from "@x-projects/app/routes/account.routes";

// Load env variables
dotenv.config({
  path: appRootPath.resolve(`.${process.env.NODE_ENV}.env`),
});

// Validate environment variables
const result = appProcessEnvValidationSchema.options({ allowUnknown: true }).validate(process.env);

if (result.error && !isEmpty(result.error.details)) {
  throw new Error(result.error.details.at(0)?.message ?? "Process env variable validation error");
}

const app: express.Express = express();

// Add request body parsers
app.use(bodyParser.json());
app.use(express.static("public"));

// Enable cors
app.use(cors());

// Use routes
app.use("/api/v1/", accountRouter);

// Add global error handler
app.use(errorHandler);

// Connect to DB
mongoose
  .connect(process.env.MONGO_DB_URI, {
    dbName: "x_projects",
  })
  .then(() => console.log("Connected to MongoDB"));

// Start server
app.listen(process.env.PORT, () => {
  console.log(`[Server]: Server is running at http://localhost:${process.env.PORT}`);
});
