import * as path from "node:path";
import * as crypto from "node:crypto";
import * as process from "node:process";
import * as jwt from "jsonwebtoken";
// Utils
import { eq, isNil, isNull, isString } from "lodash";
import { deleteFile, publicDir } from "@x-projects/app/utils/file-upload.util";
import {
  NotFoundException,
  BadRequestException,
  UnauthorizedException,
} from "@x-projects/app/utils/exceptions";
import { HTTP_STATUS } from "@x-projects/app/utils/constants/http-status.constant";
import { HttpException } from "@x-projects/app/utils/exceptions/http-exception";
// Models
import { UserModel } from "@x-projects/app/models/user.model";
// Types
import type { NextFunction, Response } from "express";
import type {
  EditAccount,
  JwtAuthPayload,
  ListUsers,
  LogIn,
  Request,
  Signup,
} from "@x-projects/app/interfaces";
import type {
  User,
  UserWithPassword,
} from "@x-projects/app/interfaces/entities/user-entity.interface";

export class AccountController {
  private static createAccessToken(user: User): string {
    return jwt.sign(
      {
        id: user.id,
        email: user.email,
      } satisfies JwtAuthPayload,
      process.env.JWT_ACCESS_TOKEN_SECRET,
      {
        algorithm: "HS256",
        expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
      },
    );
  }

  /** Auth endpoints' handlers **/
  public static async signup(
    req: Request<Signup.ReqBody>,
    res: Response<Signup.ResBody>,
    next: NextFunction,
  ): Promise<void> {
    try {
      // Check if the email is busy
      const userWithProvidedEmail = await UserModel.findOne({ email: req.body.email.trim() });

      if (!isNil(userWithProvidedEmail)) {
        throw new BadRequestException("Пользователь c указанной эл. почтой уже зарегистрирован");
      }
      const user = new UserModel({
        name: req.body.name,
        email: req.body.email,
        password: crypto.createHash("md5").update(req.body.password).digest("hex"),
        sex: req.body.sex.toUpperCase(),
        birthday: new Date(req.body.birthday).toISOString(),
        imagePath: isNil(req.file) ? null : `users/${req.file!.filename}`,
      });
      // Save user to DB
      await user.save();

      res.status(HTTP_STATUS.OK).send({ success: true });
    } catch (exc) {
      if (isString(req.file?.path)) {
        deleteFile(req.file!.path);
      }
      //
      if (exc instanceof HttpException) {
        return next(exc);
      }
      next(
        new BadRequestException("При регистрации произошла неожиданная ошибка. Попробуйте позднее"),
      );
    }
  }

  public static async login(
    req: Request<LogIn.ReqBody>,
    res: Response<LogIn.ResBody>,
    next: NextFunction,
  ): Promise<void> {
    try {
      const user = await UserModel.findOne({ email: req.body.email });

      // Check user existence
      if (isNil(user)) {
        throw new NotFoundException("Пользователь c указанной эл. почтой не найден");
      }
      // Check user password
      const passwordHash = crypto.createHash("md5").update(req.body.password).digest("hex");

      if (!eq(user.password, passwordHash)) {
        throw new UnauthorizedException("Неправильный логин, или пароль");
      }

      res.status(HTTP_STATUS.OK).send({
        user: user as User,
        accessToken: AccountController.createAccessToken(user),
      });
    } catch (exc) {
      if (exc instanceof HttpException) {
        return next(exc);
      }
      next(
        new BadRequestException("При авторизации произошла неожиданная ошибка. Попробуйте позднее"),
      );
    }
  }

  public static async checkAuth(
    req: Request,
    res: Response<LogIn.ResBody>,
    next: NextFunction,
  ): Promise<void> {
    try {
      const user = await UserModel.findById(req.user?.id);

      if (isNil(user)) {
        throw Error();
      }
      res.status(HTTP_STATUS.OK).send({
        user: user as User,
        accessToken: AccountController.createAccessToken(user),
      });
    } catch (exc) {
      next(new UnauthorizedException("Не авторизован"));
    }
  }

  /** Account endpoints' handlers **/
  public static async editAccount(
    req: Request<EditAccount.ReqBody>,
    res: Response<EditAccount.ResBody>,
    next: NextFunction,
  ): Promise<void> {
    try {
      const user = await UserModel.findById(req.user!.id);

      if (isNil(user)) {
        throw new NotFoundException("Пользователь c указанной эл. почтой не найден");
      }
      const updateData: Partial<
        Record<keyof UserWithPassword, UserWithPassword[keyof UserWithPassword]>
      > = {};
      const oldImagePath = user.imagePath;
      const newImagePath = isNil(req.file) ? null : `users/${req.file!.filename}`;

      if (!isNil(req.body.name)) {
        updateData.name = req.body.name.trim();
      }
      if (!isNil(req.body.password)) {
        updateData.password = crypto.createHash("md5").update(req.body.password).digest("hex");
      }
      if (!isNil(req.file)) {
        updateData.imagePath = isNull(newImagePath) ? user.imagePath : newImagePath;
      }
      await UserModel.updateOne(
        {
          _id: req.user!.id,
        },
        {
          $set: updateData,
        },
      );

      if (isString(newImagePath) && isString(oldImagePath)) {
        deleteFile(path.join(publicDir, oldImagePath));
      }
      const updatedUser = await UserModel.findById(req.user!.id);

      if (isNil(updatedUser)) {
        throw Error();
      }
      //
      res.status(HTTP_STATUS.OK).send(updatedUser as User);
    } catch (exc) {
      if (isString(req.file?.path)) {
        deleteFile(req.file!.path);
      }
      //
      if (exc instanceof HttpException) {
        return next(exc);
      }
      next(
        new BadRequestException(
          "При обновлении профиля произошла неожиданная ошибка. Попробуйте позднее",
        ),
      );
    }
  }

  public static async listUsers(
    req: Request,
    res: Response<ListUsers.ResBody>,
    next: NextFunction,
  ): Promise<void> {
    try {
      const [users, totalUsers] = await Promise.all([
        UserModel.find({ _id: { $ne: req.user!.id } }),
        UserModel.countDocuments({ _id: { $ne: req.user!.id } }),
      ]);
      //
      res.status(HTTP_STATUS.OK).send({
        total: totalUsers,
        rows: users.map((user) => ({
          id: user.id,
          name: user.name,
          birthday: user.birthday,
          imagePath: user.imagePath,
        })),
      });
    } catch (_) {
      next(
        new BadRequestException(
          "При получении списка пользователей произошла неожиданная ошибка. Попробуйте позднее",
        ),
      );
    }
  }
}
