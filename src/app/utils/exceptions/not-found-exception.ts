import { HttpException } from "@x-projects/app/utils/exceptions/http-exception";
import { HTTP_STATUS } from "@x-projects/app/utils/constants/http-status.constant";

export class NotFoundException extends HttpException {
  constructor(message?: HttpException["message"]) {
    super(HTTP_STATUS.NOT_FOUND, message ?? "NotFoundException");
  }
}
