import { IHttpException } from "@x-projects/app/interfaces";

export class HttpException implements IHttpException {
  constructor(
    public readonly status: IHttpException["status"],
    public readonly message: IHttpException["message"],
  ) {}
}
