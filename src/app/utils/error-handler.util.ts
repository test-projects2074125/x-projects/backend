// Utils
import { isArray, isEmpty } from "lodash";
import { HttpException } from "@x-projects/app/utils/exceptions/http-exception";
import { ValidationError } from "express-validation";
// Types and Enums
import type { NextFunction, Request, Response } from "express";
import type { ValidationErrorFunction } from "joi";
import { HTTP_STATUS } from "@x-projects/app/utils/constants/http-status.constant";

export function setValidationErrorMessage(message: string): ValidationErrorFunction {
  return (errors) => {
    for (let i = 0; i < errors.length; i++) {
      errors[i]!.message = message;
    }
    return errors;
  };
}

export function errorHandler(
  err: HttpException | Error,
  _req: Request,
  res: Response,
  next: NextFunction,
) {
  if (res.headersSent) {
    return next(err);
  }
  if (err instanceof ValidationError) {
    if (isArray(err.details.body) && !isEmpty(err.details.body)) {
      return res.status(HTTP_STATUS.UNPROCESSABLE_ENTITY).json({
        message: "Ошибка валидации",
        errors: err.details.body
          .map((error) => ({
            [error.context?.key ?? (error.path[0] as string)]: error.message,
          }))
          .reduce((accum, curr) => {
            return { ...accum, ...curr };
          }),
      });
    }
    return res.status(HTTP_STATUS.UNPROCESSABLE_ENTITY).json({
      message: "Ошибка валидации",
      errors: [],
    });
  }
  if (err instanceof HttpException) {
    return res.status(err.status).json({ message: err.message });
  }
  res.sendStatus(HTTP_STATUS.INTERNAL_SERVER_ERROR);
}
