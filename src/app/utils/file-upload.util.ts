import * as fs from "node:fs";
import * as path from "node:path";
import * as appRootPath from "app-root-path";
import * as multer from "multer";

export const publicDir = appRootPath.resolve("public");
export const usersImagesPath = path.join(publicDir, "users");

// Ensure that directories exist
if (!fs.existsSync(publicDir)) {
  fs.mkdirSync(publicDir);
}
if (!fs.existsSync(usersImagesPath)) {
  fs.mkdirSync(usersImagesPath);
}

const storage = multer.diskStorage({
  destination: (_req, _file, cb) => {
    cb(null, appRootPath.resolve("public/users"));
  },
  filename: (_req, file, cb) => {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

export function deleteFile(path: string) {
  fs.rm(path, (err) => {
    if (err) {
      console.log(err);
    }
  });
}

export const UserImageMulter = multer({ storage });
