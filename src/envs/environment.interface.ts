import * as Joi from "joi";

export enum Environment {
  Development = "development",
  Production = "production",
  Test = "test",
}

// Process environment variables
export interface AppProcessEnvVariables {
  NODE_ENV: Environment;
  PORT: number;
  MONGO_DB_URI: string;
  JWT_ACCESS_TOKEN_SECRET: string;
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: string;
}

export const appProcessEnvValidationSchema: Joi.Schema = Joi.object({
  NODE_ENV: Joi.string().valid(Environment.Test, Environment.Development, Environment.Production),
  PORT: Joi.number().required(),
  MONGO_DB_URI: Joi.string().required(),
  JWT_ACCESS_TOKEN_SECRET: Joi.string().required(),
  JWT_ACCESS_TOKEN_EXPIRATION_TIME: Joi.string().required(),
} as Required<{ [Key in keyof AppProcessEnvVariables]: Joi.AnySchema }>);
