import { AppProcessEnvVariables } from "@x-projects/envs/environment.interface";
import { JwtAuthPayload } from "@x-projects/app/interfaces";

declare global {
  namespace NodeJS {
    interface ProcessEnv extends AppProcessEnvVariables {}
  }
}

declare module "express" {
  export interface Request {
    user?: JwtAuthPayload;
  }
}
export {};
